# Typing Speed Testing Game

## General Information
This project creates a Typing Speed Test Game using Tkinter library of python

## Setup
```
$ git clone (this repository)
$ pip install tkinter
$ python typingspeed.py
```

## Game Features

* The game has a Timer of 60 seconds.
* As soon as you press ENTER it starts the timer.
* It will display a text.
* You have to enter the same text to get a 100% score,
* Otherwise it calculates the score.
* When you press ENTER new text is displayed.
* It provides the Number of Characters you got correct and missed.
* You can also see your Speed(characters/second) and Accuracy.
