
def get_sentences():
    """
    The sentences that we are using for testing the typing speed
    from a separate file
    """
    sentences = []
    try:
        with open("sentences.txt", "r") as file:
            lines = file.readlines()
            for line in lines:
                line = line.strip("\n")
                sentences.append(line)
    except FileNotFoundError:
        print("Error: FileNotFound")
    return sentences
