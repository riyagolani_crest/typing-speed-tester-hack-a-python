import tkinter as tk
import random
from math import ceil
from difflib import SequenceMatcher
from tkinter import messagebox as mb
from sentences import get_sentences

# Get sentences to be displayed
sentences = get_sentences()


class Application(tk.Frame):
    def __init__(self, root, condition_variable):
        """
        Initializes the frame

        Args:
            root (object): Frame object
            condition_variable (dict): Passes condition to run frame
        """
        tk.Frame.__init__(self, root)
        self.condition_variable = condition_variable
        print(self.condition_variable)

    def initialize(self, root):
        """
        Initialize all the widgets of the Frame
        And variables to be used for calculation

        Args:
            root (object): Frame object
        """
        self.root = root
        self.score = self.count = self.accuracy = self.totalchar = 0
        self.timeleft = 10
        self.sliderWords = ""
        self.miss = -10
        self.fontLabel = self.initialise_label(
            "Welcome to Typing Speed Tester Game", 10, 10
        )
        # Label that Contains Score keyword
        self.scoreLable = self.initialise_label("Score:", 10, 100)
        # Label that contains Live score
        self.scoreLableCount = self.initialise_label(self.score, 50, 170)
        # Label that conatins Timer Keyword
        self.timerLabel = self.initialise_label("Timer:", 650, 100)
        # Label that displays Live Timer
        self.timeLableCount = self.initialise_label(self.timeleft, 680, 180)
        # Label that will display content to type
        self.wordLabel = self.initialise_label(
            "Press Enter to read instructions", 170, 210
        )
        # Label to type content, disabled by default
        self.wordEntry = tk.Entry(
            self.root,
            font=("airal", 25, "italic bold"),
            bd=10,
            justify="center",
            state="disabled",
        )
        self.wordEntry.place(x=200, y=290)
        self.wordEntry.focus_set()
        # Button to Restart the Game
        self.optionretryLabel = self.initialise_button(
            "Retry", self.retry, 220, 550
        )
        # Button to Exit the Game
        self.optionexitLabel = self.initialise_button(
            "Exit", self.exitkey, 520, 550
        )
        # Bind Enter event with the main function
        self.root.bind("<Return>", self.startGame)

    def initialise_label(self, label_text, x, y):
        """
        To initialize all labels at a single place

        Args:
            label_text (string): Label Content to be displayed
            x (int): Position of X coordinate
            y (int): Position of Y coordinate

        Returns:
            object: Label object
        """
        self.label = tk.Label(
            self.root,
            text=label_text,
            font=("airal", 25, "italic bold"),
            bg="#663300",
            fg="yellow",
        )
        self.label.place(x=x, y=y)
        return self.label

    def initialise_button(self, button_text, func, x, y):
        """
        To initialize all buttons at a single place

        Args:
            button_text (string): Button Content to be displayed
            func (object): Function to be bound with
            x (int): Position of X coordinate
            y (int): Position of Y coordinate

        Returns:
            object: Button object
        """
        self.button = tk.Button(
            self.root,
            text=button_text,
            font=("arial", 25),
            fg="#663300",
            bg="wheat",
        )
        self.button.bind("<Button-1>", func)
        self.button.place(x=x, y=y)
        return self.button

    def startGame(self, event):
        """
        The game scoring, timer starts here
        """
        self.wordEntry.configure(state="normal")
        """  Live Timer  """
        if self.timeleft == 10:
            mb.showinfo(
                "Instructions",
                "\tTime = 60 sec \n\
                Score = Part of sentence matched \n\
                Hit = score \n\
                Miss = Part of sentence mismatched \n\
                Accuracy = average accuracy of typing \n\
                Speed = Number of character typed/ sec \n\
                Press Enter to submit\nPress OK to Start Game \
                ",
            )  # Show instructions dialog box
            self.wordLabel.configure(
                text=sentences[0]
            )  # Display first sentence
            self.time()

        """  Match the main text with the text input by user  """
        self.maintext_sentence = self.wordLabel["text"]
        self.inputtext_sentence = self.wordEntry.get()
        self.totalchar += len(self.inputtext_sentence)
        local_accuracy = SequenceMatcher(
            None, self.maintext_sentence, self.inputtext_sentence
        ).ratio()  # Calculate accuracy of each sentence
        self.score += int(local_accuracy * 10)  # Calculate Score
        self.scoreLableCount.configure(text=self.score)
        self.miss += 10 - int(ceil(local_accuracy * 10))  # Calculate Miss
        self.accuracy += local_accuracy  # Calculate Total Accuracy
        if self.score > 1:
            self.fontLabel.configure(text="")  # Remove welcome content
        self.count += 1
        random.shuffle(sentences)  # Shuffle sentences
        self.wordLabel.configure(text=sentences[0])  # Reset sentence
        self.wordEntry.delete(0, tk.END)  # Clear entry after submit

    def time(self):
        """
        Live timer
        """
        if self.timeleft >= 11:
            pass
        else:
            # For time less than 10 make timer red
            self.timeLableCount.configure(fg="red")
        if self.timeleft > 0:
            # Decrease timer every second and replace it's value
            self.timeleft -= 1
            self.timeLableCount.configure(text=self.timeleft)
            self.timeLableCount.after(1000, self.time)
        else:
            # Once game finishes, disable text entry
            self.wordEntry.configure(state="disabled")
            self.wordLabel.configure(text="Retry to play again")
            # Label that gives score
            self.gamePlayedHitLabel = self.initialise_label(
                "HIT : {}".format(self.score), 20, 450
            )
            # Label that shows wrong/ missed
            self.gamePlayedMissLabel = self.initialise_label(
                "MISS : {}".format(self.miss), 480, 450
            )
            # Label that shows speed(char/second)
            self.gamePlayedtotalScoreLabel = self.initialise_label(
                "SPEED : {:.2f} char/s".format(self.totalchar / 10), 20, 500
            )
            # Label that shows accuracy
            self.gamePlayedAccuracyLabel = self.initialise_label(
                "ACCURACY : {:.2f}%".format(
                    (self.accuracy / self.count) * 100
                ),
                480,
                500,
            )
            # Unbind Enter event from the frame
            self.root.unbind("<Return>")

    def retry(self, event):
        """
        To restart the game

        Args:
            event (object): Event handled to restart when pressed Left-click
                            on widget
        """
        print("Retry:", self.condition_variable)
        self.root.destroy()

    def exitkey(self, event):
        """
        To exit the game

        Args:
            event (object): Event handled to exit when pressed Left-click
                            on widget
        """
        self.condition_variable["run"] = "False"
        self.root.destroy()
        print("Exit:", condition_variable)


def main():
    """
    Main function that creates frame
    """
    while condition_variable["run"] == "True":
        root = tk.Tk()
        root.geometry("850x700+300+20")
        root.configure(bg="#663300")
        root.title("Typing Speed Tester Game")
        app = Application(root, condition_variable)
        app.initialize(root)
        root.mainloop()


if __name__ == "__main__":
    condition_variable = {"run": "True"}
    main()
